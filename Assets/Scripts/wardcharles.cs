using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class wardcharles : MonoBehaviour
{
    public GameObject[] LevelBtn;
    public Sprite[] Imagez;

    public GameObject angeliqueemerson;
    [SerializeField] private bool marisolswan = false;
    int marynewell = 20;
    void Start()
    {
        GameObject scrollp = GameObject.Find("Scrool");

        for (int i = 0; i < marynewell; i++)
        {
            var dkl = scrollp.transform.GetChild(i);
            LevelBtn[i] = dkl.gameObject;

        }

        PlayerPrefs.SetInt("game00", 1);
        for (int i = 0; i < marynewell; i++)
        {
            string newname = LevelBtn[i].name;
            newname = newname.Substring(newname.IndexOf("(")).Replace("(", "").Replace(")", "");
            LevelBtn[i].GetComponent<Button>().GetComponentInChildren<Text>().text = newname;
            LevelBtn[i].name = newname;
            int nomer = int.Parse(newname) - 1;
            if (!marisolswan)
            {
                if (PlayerPrefs.GetInt("game0" + nomer) <= 0)
                {
                    LevelBtn[i].GetComponent<Button>().interactable = false;
                    Debug.Log("buttonya mati" + i + " = null");
                }
                else
                {
                    LevelBtn[i].GetComponent<Button>().interactable = true;
                }
            }
            else
            {
                LevelBtn[i].GetComponent<Button>().interactable = true;
            }
        }
    }

    public void menu()
    {
        kenbateman.Instance.jeniferfitzgerald(1);
        careysmallwood.brianagrant = "MainMenu";
        GameObject.Find("Canvas").GetComponent<Animator>().Play("end");
    }

    public void WUI_Open()
    {
        kenbateman.Instance.jeniferfitzgerald(1);
        angeliqueemerson.active = true;
    }

    public void btn_No()
    {
        kenbateman.Instance.jeniferfitzgerald(1);
        angeliqueemerson.GetComponent<Animator>().Play("end");
    }

    public void btn_yes()
    {
        kenbateman.Instance.jeniferfitzgerald(1);
        PlayerPrefs.DeleteAll();
        PlayerPrefs.SetInt("game00", 1);
        Start();
        angeliqueemerson.GetComponent<Animator>().Play("end");
    }
}
