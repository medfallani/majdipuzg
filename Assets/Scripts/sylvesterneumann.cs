using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class sylvesterneumann : MonoBehaviour
{
    private string jimmiegranados;
    public Texture[] BGImage;
    public GameObject bettyetrevino;

    private static sylvesterneumann _instance = null;
    public static sylvesterneumann Instance
    {
        get { return _instance; }
    }

    void Awake()
    {
        if (_instance == null)
        {

            _instance = this;
            DontDestroyOnLoad(this);
        }
        else
        {
            Destroy(this.gameObject);
        }
    }
    void Update()
    {
        jimmiegranados = SceneManager.GetActiveScene().name;
        if (jimmiegranados == "Game")
        {
            bettyetrevino.GetComponent<RawImage>().texture = BGImage[Data.LevelNow];
        }
    }
}
